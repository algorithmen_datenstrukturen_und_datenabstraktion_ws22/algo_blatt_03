from Aufgabe03a import Stack

def end(c):
    if c == "(": return ")"
    if c == "[": return "]"
    if c == "<": return ">"
    if c == "{": return "}"
    return None

def check_klammern(s):
    check_sum = Stack()
    for c in s:
        if c in ["(", "[", "<", "{"]:
            check_sum.push(c)
        elif c in [")", "]", ">", "}"]:
            if check_sum.isEmpty():
                return False
            elif c == end(check_sum.pop()):
                continue
            else:
                return False
    return check_sum.isEmpty()

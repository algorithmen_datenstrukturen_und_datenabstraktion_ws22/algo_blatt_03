class Stack():

    def __init__(self, l=[]):
        self.stack = l

    def __str__(self):
        return str(self.stack)

    def push(self, element):
        self.stack.append(element)

    def pop(self):
        if self.isEmpty():
            raise ValueError("The Stack is empty!")
        element = self.stack[-1]
        del self.stack[-1]
        return element

    def top(self):
        return self.stack[-1]

    def size(self):
        return len(self.stack)

    def isEmpty(self):
        return not bool(self.stack)

class ListElement:

    def __init__(self, value, next=None):
        self.value = value
        self.next = next

class StackList:

    def __init__(self, l=None):
        if l is None:
            self.first_element = None
        else:
            self.first_element = ListElement(l[-1])
            element = self.first_element
            for value in l[1::-1]:
                while element.next is not None:
                    element = element.next
                element.next = ListElement(value)

    def __str__(self):
        if self.isEmpty():
            return "StackList()"

        return_string = ""
        element = self.first_element
        while element.next is not None:
            return_string += str(element.value) + " -> "
            element = element.next

        return return_string + str(element.value)

    def push(self, value):
        self.first_element = ListElement(value, self.first_element)

    def pop(self):
        if self.isEmpty():
            raise ValueError("The Stack is empty!")
        value = self.first_element.value
        self.first_element = self.first_element.next
        return value

    def top(self):
        return self.first_element.value

    def size(self):
        if self.first_element is None:
            return 0

        n = 1
        element = self.first_element
        while element.next is not None:
            n += 1
            element = element.next

        return n

    def isEmpty(self):
        return self.first_element is None

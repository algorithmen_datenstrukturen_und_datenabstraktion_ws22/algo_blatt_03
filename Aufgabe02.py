def find_min_max(A):
    curr_min = A[0]
    curr_max = A[0]
    for i in A[1:]:
        if i < curr_min:
            curr_min = i
        elif i > curr_max:
            curr_max = i
    return curr_min, curr_max

def find_min_max_recursive(A, l=0, r=None):
    if r is None:
        r = len(A)

    if r - l == 1:
        return A[l], A[l]

    min1, max1 = find_min_max_recursive(A, l, (l+r)//2)
    min2, max2 = find_min_max_recursive(A, (l+r)//2, r)

    if min1 < min2:
        minA = min1
    else:
        minA = min2

    if max1 > max2:
        maxA = max1
    else:
        maxA = max2

    return minA, maxA
